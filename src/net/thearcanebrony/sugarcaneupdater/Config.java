package net.thearcanebrony.sugarcaneupdater;

import net.thearcanebrony.sugarcaneupdater.sourcetype.Source;

public class Config {
    public static String OutputFile = "server.jar";
    public static Source ServerSource = Sources.SUGARCANE_RELEASE;
    public static PersistentConfig PersistentConfig = net.thearcanebrony.sugarcaneupdater.PersistentConfig.Get();
    public static LocalState LocalState = new LocalState();
    public static boolean Verbose = false;
    public static boolean LogIO = false;
    public static boolean ForceDownload = false;
    public static boolean DownloadJava = true;
    public static boolean SystemJava = false;
    public static String LaunchMem = "1G";
    public static String LaunchPreMem = "1G";
    public static String ServerArgs = "";
    public static String JvmArgs = "";
    public static boolean AikarFlags = true;
}
