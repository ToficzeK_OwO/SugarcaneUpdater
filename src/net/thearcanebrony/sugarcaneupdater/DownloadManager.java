package net.thearcanebrony.sugarcaneupdater;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DecimalFormat;
import java.util.Arrays;

public class DownloadManager {
    private static final DecimalFormat df = new DecimalFormat("#.##");
    private static long totalBytes = 0;

    public static String GetUrlResponse(String _url) {
        Console.logDebug(String.format("Sending HTTP request to %s!", _url));
        try {
            URL url = new URL(_url);
            HttpURLConnection con = (HttpURLConnection) url.openConnection();
            con.setRequestMethod("GET");
            InputStream is = con.getInputStream();
            BufferedReader rd = new BufferedReader(new InputStreamReader(is));
            StringBuilder response = new StringBuilder();
            String line;
            while ((line = rd.readLine()) != null) {
                response.append(line);
                response.append('\n');
            }
            rd.close();
            Console.LogIO(response.toString());
            return response.toString();
        } catch (IOException e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            Console.WriteLine("Something went wrong!\n" + sw);
            return "Something went wrong! " + e;
        }
    }
    static boolean running = true;
    public static long DownloadFileNew(String _url, String outfile) {
        boolean isConsole = System.console() != null && !Files.exists(Paths.get("/.dockerenv"));
        Console.logDebug("Console: " + isConsole);
        long filesize = getFileSize(_url);
        try {
            totalBytes = 0;
            BufferedInputStream in = new BufferedInputStream(new URL(_url).openStream());
            FileOutputStream fileOutputStream = new FileOutputStream(outfile);
            byte[] dataBuffer = new byte[1024];
            int bytesRead;
            Thread thr = new Thread(() -> {
                int cooldown = isConsole ? Config.LogIO ? 1 : 50 : 1000;
                String lineSeparator = "              " + (isConsole ? "\r" : "\r\n");
                double updates_per_second = 1000d / cooldown;
                double[] _downloadSpeed = new double[(int) Math.ceil(updates_per_second)];
                int i = 0;
                int spinnerPosition = 0;
                long lastUpdate = 0;
                while (running) {
                    String sp = isConsole ? (Static.Spinner[spinnerPosition++ % Static.Spinner.length] + " ") : "";
                    _downloadSpeed[(int) (i++ % updates_per_second)] = totalBytes - lastUpdate;
                    double downloadSpeed = Arrays.stream(_downloadSpeed).sum();
                    Console.Write(String.format("%sRead %s at %s/s",sp, Util.FormatSizeProgress(filesize, totalBytes), Util.FormatSize((long) downloadSpeed/8)) + lineSeparator);
                    lastUpdate = totalBytes;
                    Util.sleep(cooldown);
                }
            }); thr.start();
            while ((bytesRead = in.read(dataBuffer, 0, dataBuffer.length)) != -1) {
                fileOutputStream.write(dataBuffer, 0, bytesRead);
                totalBytes += bytesRead;
                if(!thr.isAlive()) thr.start();
            }
            running = false;
            Console.WriteLine(String.format("Download complete! Downloaded %s into %s!", Config.LogIO ? (totalBytes + " bytes") : (String.format("%.2f", filesize / 1024d / 1024d) + " MB"), outfile));
            return totalBytes;
        } catch (IOException e) {
            StringWriter sw = new StringWriter();
            PrintWriter pw = new PrintWriter(sw);
            e.printStackTrace(pw);
            Console.WriteLine("Something went wrong!\n" + sw);
            return totalBytes;
        }
    }

    private static long getFileSize(String _url) {
        URLConnection conn = null;
        try {
            URL url = new URL(_url);
            conn = url.openConnection();
            if (conn instanceof HttpURLConnection) {
                ((HttpURLConnection) conn).setRequestMethod("HEAD");
            }
            conn.getInputStream();
            return conn.getContentLengthLong();
        } catch (IOException e) {
            throw new RuntimeException(e);
        } finally {
            if (conn instanceof HttpURLConnection) {
                ((HttpURLConnection) conn).disconnect();
            }
        }
    }
}
