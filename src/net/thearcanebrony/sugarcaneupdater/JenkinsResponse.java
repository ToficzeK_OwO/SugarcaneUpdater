package net.thearcanebrony.sugarcaneupdater;

import com.google.gson.Gson;

import java.util.List;

public class JenkinsResponse {
    public static JenkinsResponse Get(String json){
        Gson g = new Gson();
        return g.fromJson(json, JenkinsResponse.class);
    }
    public String _class;
    public List<Action> actions;
    public List<Artifact> artifacts;
    public boolean building;
    public String description;
    public String displayName;
    public int duration;
    public int estimatedDuration;
    public Object executor;
    public String fullDisplayName;
    public String id;
    public boolean keepLog;
    public int number;
    public int queueId;
    public String result;
    public long timestamp;
    public String url;
    public List<ChangeSet> changeSets;
    public List<Culprit> culprits;
    public Object nextBuild;
    public PreviousBuild previousBuild;
    public class Caus{
        public String _class;
        public String shortDescription;
    }

    public class Branch{
        public String SHA1;
        public String name;
    }

    public class Marked{
        public String SHA1;
        public List<Branch> branch;
    }

    public class Revision{
        public String SHA1;
        public List<Branch> branch;
    }

    public class OriginMain{
        public String _class;
        public int buildNumber;
        public Object buildResult;
        public Marked marked;
        public Revision revision;
    }

    public class LastBuiltRevision{
        public List<Branch> branch;
    }

    public class Action{
        public String _class;
        public List<Caus> causes;
        public LastBuiltRevision lastBuiltRevision;
        public List<String> remoteUrls;
        public String scmName;
    }

    public class Artifact{
        public String displayPath;
        public String fileName;
        public String relativePath;
    }

    public class Author{
        public String absoluteUrl;
        public String fullName;
    }

    public class Path{
        public String editType;
        public String file;
    }

    public class Item{
        public String _class;
        public List<String> affectedPaths;
        public String commitId;
        public long timestamp;
        public Author author;
        public String authorEmail;
        public String comment;
        public String date;
        public String id;
        public String msg;
        public List<Path> paths;
    }

    public class ChangeSet{
        public String _class;
        public List<Item> items;
        public String kind;
    }

    public class Culprit{
        public String absoluteUrl;
        public String fullName;
    }

    public class PreviousBuild{
        public int number;
        public String url;
    }
}