package net.thearcanebrony.sugarcaneupdater;

import com.google.gson.Gson;

import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;

public class PersistentConfig {
    private static final Gson g = new Gson();
    public LocalState LocalState = new LocalState();

    public static PersistentConfig Get() {
        if (!Files.exists(Paths.get("SugarcaneUpdater.json"))) return new PersistentConfig();
        else return g.fromJson(FileU.ReadAllText("SugarcaneUpdater.json"), PersistentConfig.class);
    }

    public void Save() {
        try {
            FileWriter myWriter = new FileWriter("SugarcaneUpdater.json");
            myWriter.write(g.toJson(this));
            myWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
