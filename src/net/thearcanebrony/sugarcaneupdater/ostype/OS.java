package net.thearcanebrony.sugarcaneupdater.ostype;

import net.thearcanebrony.sugarcaneupdater.Console;

import java.nio.file.Files;
import java.nio.file.Paths;

public class OS {
    public static OS getOS(){
        if(Files.exists(Paths.get("C:\\Windows\\System32\\cmd.exe"))) return new Windows();
        else if(Files.exists(Paths.get("/bin/bash"))) return new Linux();
        else {
            Console.WriteLine("Could not detect OS! Please report this to The Arcane Brony!");
            return new OS();
        }
    }
    public String Name = "Unknown OS, assuming Unix-compatible";
    public String OsType = "unknown";
    public String[] ShellCmd = {"bash", "-c"};
}
