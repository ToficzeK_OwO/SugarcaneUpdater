package net.thearcanebrony.sugarcaneupdater.sourcetype;

import net.thearcanebrony.sugarcaneupdater.Config;
import net.thearcanebrony.sugarcaneupdater.Console;
import net.thearcanebrony.sugarcaneupdater.JenkinsResponse;
import net.thearcanebrony.sugarcaneupdater.LocalState;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.Paths;

import static net.thearcanebrony.sugarcaneupdater.DownloadManager.DownloadFileNew;
import static net.thearcanebrony.sugarcaneupdater.DownloadManager.GetUrlResponse;

public class JenkinsSource extends Source {

    public JenkinsSource(String name, String url) {
        super(name, url);
    }
    JenkinsResponse lastJenkinsResponse;
    @Override
    public void DownloadLatest(String outfile) {
        if(!CheckUpdate()){
            Console.WriteLine(Name + " is already up to date, not downloading!");
            return;
        }
        JenkinsResponse jenkinsResponse = lastJenkinsResponse;
        LocalState.Build b = Config.LocalState.builds.get(Name);
        JenkinsResponse.Artifact artifact = jenkinsResponse.artifacts.get(0);
        String artifactUrl = Url + "/artifact/" + artifact.relativePath + "?t=" + jenkinsResponse.number;
        Console.logDebug("Artifact URL: " + artifactUrl);
        Console.WriteLine(String.format("Got build info!\nDownloading latest %s jar...", Name));
        long size;
        if(Name.equals("SugarcaneUpdater")) {
            size = DownloadFileNew(artifactUrl, "tmp.jar");
            try {
                if(Files.deleteIfExists(Paths.get("SugarcaneUpdater.jar")))
                    Files.move(Paths.get("tmp.jar"), Paths.get("SugarcaneUpdater.jar"));
            } catch (IOException e) {
                Console.WriteLine(
                    "Failed to update SugarcaneUpdater! The file could not be deleted, in order to self-update." +
                            "\nThis may be due to OS restrictions." +
                            "\nHow to enable updating SugarcaneUpdater (add these to your startup script):" +
                            "\n - Windows: move /Y tmp.jar SugarcaneUpdater.jar" +
                            "\n - Unix: mv tmp.jar SugarcaneUpdater.jar" +
                            "\nKeep in mind you will continue to see this error after doing this, but it is safe to ignore!");
            }
        }
        else size = DownloadFileNew(artifactUrl, outfile);
        Console.LogIO("New download read: " + size);
        b.Number = jenkinsResponse.number;
        b.OutFile = outfile;
        if(!Name.equals("SugarcaneUpdater")) Config.LocalState.LatestPull = Name;
    }
    @Override
    public boolean CheckUpdate(){
        Console.WriteLine(String.format("Fetching build info for %s...", Name));
        String resp = GetUrlResponse(Url + "/api/json");
        if(resp.contains("FileNotFoundException")){
            Console.WriteLine("Failed to get build info, does this server/branch exist? Exiting!");
            System.exit(1);
            return false;
        }
        JenkinsResponse jenkinsResponse = JenkinsResponse.Get(resp);
        lastJenkinsResponse = jenkinsResponse;
        if(!Config.LocalState.builds.containsKey(Name)) Config.LocalState.builds.put(Name, new LocalState.Build());
        LocalState.Build b = Config.LocalState.builds.get(Name);
        Console.logDebug(String.format("%s -> %s", b.Number, jenkinsResponse.number));
        if(Config.LocalState.LatestPull != null && Config.LocalState.builds.get(Name).Number >= jenkinsResponse.number && Config.LocalState.LatestPull.equals(Name) && !Config.ForceDownload) {
            Console.WriteLine(Name + " is already up to date, not downloading!");
            return false;
        }
        else return true;
    }
}
