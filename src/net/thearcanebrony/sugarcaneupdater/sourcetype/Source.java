package net.thearcanebrony.sugarcaneupdater.sourcetype;

import net.thearcanebrony.sugarcaneupdater.Console;

public class Source {
    public String Name = "";
    public String Url = "";

    public Source(String name, String url) {
        Name = name;
        Url = url;
    }

    public void DownloadLatest(String file) {
        Console.WriteLine(this.getClass().getCanonicalName() + ": Download not implemented!");
    }
    public boolean CheckUpdate(){
        Console.WriteLine(this.getClass().getCanonicalName() + ": Update checking not implemented!");
        return false;
    }
}
